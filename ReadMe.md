# Goal

This will model the movement of massive objects using newtonian physics. The objects merge upon collision. Initial state is random.

# Implementation

# Usage

_Assuming Python to be installed_

1. Create a new virtual environment  
   `python -m venv venv`
2. Activate environment (assuming a sane operating system)  
    `source venv/bin/activate`
3. Install required packages  
   `pip install -r requirements.txt`  
   or (way more fun)  
   `python -m pip install $(cat requirements.txt)`
4. Run  
    `python main.py`
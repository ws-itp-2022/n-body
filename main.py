from distutils.spawn import spawn
from math import pi
from random import uniform
import vpython as vp  # For visualization

# Constants
DT: float = 0.001  # How much time passes between subsequent simulation steps
G: float = 8  # How strong gravity is supposed to be.
SIM_PER_FRAME: int = 5  # How many simulation steps to perform before updating the view

# How the simulation starts out
spawn_radius: float = 30.0  # Over what radius objects are initially distributed
spawn_velocity: float = 2.0  # Maximum starting speed
spawn_min_m: float = 1  # Minimum size
spawn_max_m: float = 7.0  # See above
spawn_n: int = 60  # How many objects there are to begin with

# Custom data types

# Contains minimum data necessary, increases cache hit rate


class sim_object:
    def __init__(self, pos: vp.vec, vel: vp.vec,  mass: float) -> None:
        self.pos = pos
        self.vel = vel
        self.mass = mass
        self.radius = pow(3*mass/(4*pi), 1/3)


# I know globals are usually a bad idea, but let's keep this simple
# It might be a good idea to replace this with numpy arrays and throw in some numba for good measure
sim_data: list[sim_object] = [sim_object(vp.vec.random(
)*spawn_radius, vp.vec.random()*spawn_velocity, uniform(spawn_min_m, spawn_max_m)) for _ in range(spawn_n)]
objects: list[vp.sphere] = [
    vp.sphere(pos=i.pos, color=vp.vec.random(), r=i.radius) for i in sim_data]


def simulate(dt: float) -> None:
    for i in range(len(sim_data)-1, -1, -1):
        a = sim_data[i]
        for j in range(i-1, -1, -1):
            b = sim_data[j]
            v_ab = a.pos-b.pos
            if(v_ab.mag < a.radius+b.radius):
                b.pos = (a.pos*a.mass+b.pos*b.mass)/(a.mass+b.mass)
                b.vel = (a.vel*a.mass+b.vel*b.mass)/(a.mass+b.mass)
                objects[j].color = (objects[i].color*a.mass +
                                    objects[j].color*b.mass)/(a.mass+b.mass)
                b.mass += a.mass
                b.radius = pow(3*b.mass/(4*pi), 1/3)
                objects[i].visible = False
                objects.remove(objects[i])
                sim_data.remove(a)
                break
            v_ab_norm = v_ab.norm()
            dist2 = v_ab.mag2
            F_dt = G*dt*(a.mass*b.mass)/dist2
            a.vel += F_dt/a.mass*-v_ab_norm
            b.vel += F_dt/b.mass*v_ab_norm
            a.pos += a.vel*dt
            b.pos += b.vel*dt


# Optimization: destroy objects here?
def update() -> None:
    for i in range(len(sim_data)):
        objects[i].pos = sim_data[i].pos


def follow():
    p = vp.scene.mouse.pick
    if p:
        vp.scene.follow(p)

# TODO: measure actual rate


def main() -> None:
    vp.scene.resizable = True
    vp.scene.bind('click',follow)
    while 1:
        vp.rate(100)
        [simulate(DT/SIM_PER_FRAME) for _ in range(SIM_PER_FRAME)]
        update()


if __name__ == "__main__":
    main()
else:
    raise RuntimeError("This is not a library, please run it directly")
